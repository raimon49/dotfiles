#!/bin/sh

asdf plugin add cf
asdf plugin add conftest
asdf plugin add container-diff
asdf plugin add golang
asdf plugin add goss
asdf plugin add k9s
asdf plugin add kubectl
asdf plugin add nodejs
asdf plugin add peco https://github.com/ryodocx/asdf-peco.git
asdf plugin add python
asdf plugin add ruby
asdf plugin add rust
asdf plugin add shellcheck
